#!/usr/bin/env python
### Addendum to gRNA sequencing pipeline : post-processing steps to: ###
    #### collapse "believeable" UMI's ###
    #### discard reads which do not overlap first 20 bases "spacer" region of gRNA ###
    
import pandas
import os
import csv
import sys
### parse table to read from cmd line input argv 1 ###
table_in = sys.argv[1]
### read in RESULTS_B2M_B2Mlike_0_5per_S13_R1_001 from old script ###
df1 = pandas.read_table(table_in,sep=',')
### reduce dataframe down to only records observed at least twice ###
new_df1 = df1[df1['SEQ.1'] > 1]
### collapse by SEQ and Read and drop excess summary columns from groupby ###
bc_counts = new_df1.groupby(['Read'])['SEQ'].nunique().reset_index()
bc_counts.columns=['Read','BCcount']
### create new dataframe that keeps all rows of 'left' table while adding records from 'right' dataframe based on column 'Read' ###
df3 = pandas.merge(new_df1 , bc_counts, on='Read', how='left')
df4 = df3[df3['BCcount'] == 1]
final_counts = df4.groupby(['SEQ'])['Read'].nunique().reset_index()
final_counts.columns=['SEQ','BCcount']
### define pairwise alignment function using Biopython ###
def alignment_pairwise2(x,y):
    ### Bipython dependency ###
    ### for most use cases "x" should be the ref/wt and "y" should be the variant to be aligned to x
    from Bio import pairwise2
    ### pairwise2 local 'ms' mode
        # CODE  DESCRIPTION
        # x     No parameters. Identical characters have score of 1, otherwise 0.
        # m     A match score is the score of identical chars, otherwise mismatch
              # score.
        # d     A dictionary returns the score of any pair of characters.
        # c     A callback function returns scores.
        # The gap penalty parameters are:
        # CODE  DESCRIPTION
        # x     No gap penalties.
        # s     Same open and extend gap penalties for both sequences.
        # d     The sequences have different open and extend gap penalties.
        # c     A callback function returns the gap penalties.
    return_list =  pairwise2.align.localms(x,y, 3, -.2, -5, -.1)
    ret_seq1 = return_list[0][0]
    ret_seq2 = return_list[0][1]
    ### extra bit below makes inserted / mismatch bases lowercase ###
    ret_seq3 = ""
    i = 0
    while i < len(ret_seq2): 
        if ret_seq2[i] != ret_seq1[i] :
            ret_seq3 = ret_seq3 + ret_seq2[i].lower()
            i = i +1
        else :
            ret_seq3 = ret_seq3 + ret_seq2[i]
            i = i + 1
    return ret_seq3;

### do alignments on all candidate sequences returned and return new appended column of alignments to ref ###
### this step will take a lot of time ###
final_counts['aln'] = final_counts.apply(lambda row: alignment_pairwise2("ATCAGAGGCCAAACCCTTCCGTTTTAGAGCTAGAAATAGCAAGTTAAAATAAGGCTAGTCCGTTATCAACTTGAAAAAGTGGCACCGAGTCGGTGCTTTTAACTGTAGGCACCATCAATCCCGGCACTGCCAGATCGGAAGAGCACACGTCTGAACTCCAGTCACGCCAATATCTCGTATGCCGTCTTCTGCTTG",row[0]),axis=1)
#final_counts['aln2'] = final_counts.apply(lambda row: alignment_pairwise2("ATATACGGAGCGACTCAAGTGTTTTAGAGCTAGAAATAGCAAGTTAAAATAAGGCTAGTCCGTTATCAACTTGAAAAAGTGGCACCGAGTCGGTGCTTTTAACTGTAGGCACCATCAATCCCGGCACTGCCAGATCGGAAGAGCACACGTCTGAACTCCAGTCACGCCAATATCTCGTATGCCGTCTTCTGCTTG",row[0]),axis=1)
### parse name of output .csv file from cmd line input###
out_table = sys.argv[2]
final_counts.to_csv(out_table)
### pare further down by eliminating all rows where both aln and aln2 are 20x "-" ??? ###
### example usage:
    ### /home/clarksc3/gRNA_sequencing_pipeline_addendum_3.py "RESULTS_B2M_B2Mlike_2per_S11_R1_001.fastq.csv" "B2M_2per.csv" ###