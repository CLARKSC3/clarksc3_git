#!/usr/bin/env python

### 1 : worklistfile with header "Sample SMF-ID"

### 2 : copy FROM directory in format /db/dmp/NGS/Data/........

### 3 : copy TO directory 

import csv 
import sys
import re
import os



cpF_dir = sys.argv[2]
cpT_dir = sys.argv[3]

with open(sys.argv[1], 'rt') as infile:
	reader = csv.DictReader(infile)
	for row in reader:
		name = row["Sample SMF-ID"]
		file_list = []
		file_list += [ each for each in os.listdir(cpF_dir+"/") if each.startswith(name) and each.find("_R1_") > 0 ]
		for file in file_list:
			print(file)
			file_int = file[:file.find("_R1_")]
			file2 = file_int + "_R2_001.fastq.gz"
			cp_file = "cp " + cpF_dir + "/" + file + " " + cpT_dir
			cp_file2 = "cp " + cpF_dir + "/" + file2 + " " + cpT_dir
			os.system(cp_file)
			os.system(cp_file2)