def translate(REF):
    CODON = "-"
    if REF=="GCT" or REF=="GCC" or REF=="GCA" or REF=="GCG" : 
        CODON = "A" 
    if REF=="CGT" or REF=="CGC" or REF=="CGA" or REF=="CGG" or REF=="AGA" or REF=="AGG" : 
        CODON = "R" 
    if REF=="AAT" or REF=="AAC" : 
        CODON = "N" 
    if REF=="GAT" or REF=="GAC" : 
        CODON = "D" 
    if REF=="TGT" or REF=="TGC" : 
        CODON = "C" 
    if REF=="CAA" or REF=="CAG" : 
        CODON = "Q" 
    if REF=="GAA" or REF=="GAG" : 
        CODON = "E" 
    if REF=="GGT" or REF=="GGC" or REF=="GGA" or REF=="GGG" : 
        CODON = "G" 
    if REF=="CAT" or REF=="CAC" : 
        CODON = "H" 
    if REF=="ATT" or REF=="ATC" or REF=="ATA" : 
        CODON = "I" 
    if REF=="TTA" or REF=="TTG" or REF=="CTT" or REF=="CTC" or REF=="CTA" or REF=="CTG" : 
        CODON = "L" 
    if REF=="AAA" or REF=="AAG" : 
        CODON = "K" 
    if REF=="ATG" : 
        CODON = "M" 
    if REF=="TTT" or REF=="TTC" : 
        CODON = "F" 
    if REF=="CCT" or REF=="CCC" or REF=="CCA" or REF=="CCG" : 
        CODON = "P" 
    if REF=="TCT" or REF=="TCC" or REF=="TCA" or REF=="TCG" or REF=="AGT" or REF=="AGC" : 
        CODON = "S" 
    if REF=="TAA" or REF=="TAG" or REF=="TGA" : 
        CODON = "!" 
    if REF=="ACT" or REF=="ACC" or REF=="ACA" or REF=="ACG" : 
        CODON = "T" 
    if REF=="TGG" : 
        CODON = "W" 
    if REF=="TAT" or REF=="TAC" : 
        CODON = "Y" 
    if REF=="GTT" or REF=="GTC" or REF=="GTA" or REF=="GTG" : 
        CODON = "V"
    return CODON;
    
def codons(seq):
    seqlen = len(seq)
    while seqlen % 3 > 0 :
        seq = seq[:seqlen-1]
        seqlen = len(seq)
    seqlist = list(seq)
    i = 0
    transeq = ""
    while i < seqlen: 
        DNA_codon = seq[i:i+3]
        P_codon = translate(DNA_codon)
        transeq = transeq + P_codon
        i = i +3
    return transeq;
    
#>>> df['trans'] = df.apply(lambda x:codons(x['seq']), axis = 1)
#>>> df
#                        seq     trans
#0  ATCGATCGATCGATCGATCGATCG  IDRSIDRS
