#!/usr/bin/env python

# positional parameters: 
    #1: .csv file 
    #2: old file name list key column header name 
    #3: new file name tag column header name 
    #4: file extension or more common trailing characters
    
    
# print(List)

import csv
import sys
import os
import re

with open(sys.argv[1], 'rt') as f:
    reader = csv.DictReader(f)
    old = sys.argv[2]
    new = sys.argv[3]
    extension = sys.argv[4]
    anchor = "_S.*_L001"
    for row in reader:
        key = row[old]
        List = []
        List += [each for each in os.listdir(".") if each.find(key) > -1 and each.endswith(extension) ]
        for file in List: 
            endText = file[re.search(anchor,file).start():]
            text = [ (row[new]) , endText ]
            newText = "".join(text)
            os.rename(file,newText)