def alignment_pairwise2(x,y):
    ### Bipython dependency ###
    ### for most use cases "x" should be the ref/wt and "y" should be the variant to be aligned to x
    from Bio import pairwise2
    ### pairwise2 local 'ms' mode
        # CODE  DESCRIPTION
        # x     No parameters. Identical characters have score of 1, otherwise 0.
        # m     A match score is the score of identical chars, otherwise mismatch
              # score.
        # d     A dictionary returns the score of any pair of characters.
        # c     A callback function returns scores.
        # The gap penalty parameters are:
        # CODE  DESCRIPTION
        # x     No gap penalties.
        # s     Same open and extend gap penalties for both sequences.
        # d     The sequences have different open and extend gap penalties.
        # c     A callback function returns the gap penalties.
    return_list =  pairwise2.align.localms(x,y, 3, -.2, -5, -.1)
    ret_seq1 = return_list[0][0]
    ret_seq2 = return_list[0][1]
    ### extra bit below makes inserted / mismatch bases lowercase ###
    ret_seq3 = ""
    i = 0
    while i < len(ret_seq2): 
        if ret_seq2[i] != ret_seq1[i] :
            ret_seq3 = ret_seq3 + ret_seq2[i].lower()
            i = i +1
        else :
            ret_seq3 = ret_seq3 + ret_seq2[i]
            i = i + 1
    return [ret_seq1,ret_seq2,ret_seq3];