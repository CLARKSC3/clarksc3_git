def rc(seq):
    complement = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A', 'a':'t', 'c':'g', 'g':'c', 't':'a'}
    reverse_complement = "".join(complement.get(base, base) for base in reversed(seq))
    return reverse_complement;


    
i = 0
j = 0
k = 0    
def mismatch(seq,ref):
    outstring = ''
    match = difflib.SequenceMatcher(None, ref, seq).get_matching_blocks()
    i = len(match)
    l_coord = match[0].size
    r_coord = match[i-2].size
    l_string = seq[0:l_coord]
    r_string = seq[-r_coord:]
    