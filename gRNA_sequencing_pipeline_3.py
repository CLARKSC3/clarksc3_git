#!/usr/bin/env python


### 1 : .csv file to read
### 2 : column header with unique file handles
### 3 : ref 1
### 4 : ref 2
### 5 : R1 length to keep
### 6 : R2 length to keep

### parameters for quality filtering etc.? ###
### routine to make bowtie index too???? ####

### FOR NOW THIS SCRIPT ASSUMES THE INDEX HAS BEEN MADE ###
#######   FIGURE OUT NAMING CONVENTIONS   #### <<<< TROUBLESHOOTING TIP!!!

### have os.sys load up bowtie2 module? 
####### figure out versioning and dependencies one way or the other ###### <<< TROUBLESHOOTING TIP!!!

### VERY CLOSE TO FUNCTIONAL : REWORK WITH SUBPROCESS ###
import csv
import sys
import fileinput 
import re
import os
import subprocess
import collections
import numpy
import pandas
import xlsxwriter
import xlrd

R1_len = sys.argv[5]
R2_len = sys.argv[6]
ref1 = sys.argv[3]
ref2 = sys.argv[4]
worklist = sys.argv[1]
os.system("module load bowtie2")

### section to read in csv worklist ###
with open(worklist,"rt") as f:
    reader = csv.DictReader(f)
    key = sys.argv[2]
    for row in reader:
        filekey = row[key]
        print(filekey)
        filelist = []
        ### this is very dependent on a truly unique list of filekeys !!!!!  ###
        filelist += [each for each in os.listdir(".") if each.find(filekey) > -1 and each.find("_R1_") > -1]
        print(filelist)
        for file in filelist:
            ### make R2 file variable ###
            file2 = file.replace("_R1","_R2")
            ### build commands for file trimming etc here ###
            file_R1_TRIM = "TRIM_" + R1_len + file
            file_R2_TRIM = "TRIM_" + R2_len + file2
            trim_r1_cmd = [ "/usr/prog/ngdx/fastx/0.0.13/bin/fastx_trimmer", "-l " + R1_len , "-i" + file , "-o" + file_R1_TRIM , "-Q33"]
            subprocess.call(trim_r1_cmd)
            trim_r2_cmd = [ "/usr/prog/ngdx/fastx/0.0.13/bin/fastx_trimmer", "-l " + R2_len , "-i" + file2 , "-o" + file_R2_TRIM , "-Q33"]
            subprocess.call(trim_r2_cmd)
            combined_file = "Combined_TRIM_" + file
            paste_cmd1 = ["paste", "-d","," , file_R1_TRIM , file_R2_TRIM]
            ###paste_cmd2 = ["paste1", ">" , combined_file]
            with open (combined_file, "wt") as combined_file_object:
                subprocess.call(paste_cmd1, stdout=combined_file_object)
            ###subprocess.call(paste_cmd2)
            ### sub-routine that cleans pasting artifacts ###
            with open(combined_file,"rt") as pasted_file:
                pasted_lines = pasted_file.readlines()
                counter1 = len(pasted_lines)
            i = 0
            clean_combined_file = "Clean_" + combined_file
            with open (clean_combined_file, "wt") as outfile:
                while i < counter1:
                    line1 = pasted_lines[i].strip("\n").split(",")[0] + "\n"
                    i = i+1
                    line2 = (pasted_lines[i].strip("\n")).replace(",","") + "\n"
                    i = i + 1
                    line3 = (pasted_lines[i].strip("\n")).split(",")[0] + "\n"
                    i = i+1
                    line4 = (pasted_lines[i].strip("\n")).replace(",","")  + "\n"
                    write_line = line1 + line2  + line3 + line4 
                    i = i +1
                    outfile.write(write_line)
            #### sub-routine to remove N reads ####
            nrem_clean_combined_file = "Nrem_" + clean_combined_file
            with open(clean_combined_file, "rt") as edit_file:
                edit_lines = edit_file.readlines()
                counter2 = len(edit_lines)
            i = 0 
            Ncount = 0 
            with open (nrem_clean_combined_file, "wt") as outfile2: 
                while i < counter2:
                    i = i + 1
                    if edit_lines[i].find("N") < (len(edit_lines[i]) -1) and edit_lines[i].find("N") > -1:
                        ##print(edit_lines[i].find("N"))
                        i = i + 3
                        Ncount = Ncount + 1
                    else : 
                        i = i -1
                        line1 = edit_lines[i]
                        i = i + 1 
                        line2 = edit_lines[i]
                        i = i + 1
                        line3 = edit_lines[i]
                        i = i + 1
                        line4 = edit_lines[i]
                        write_line = line1 + line2  + line3 + line4 
                        outfile2.write(write_line)
                        i = i + 1
            print( str(Ncount) + " N-reads removed")
            ### sub-routine to remove quality score artifacts ### 
            with open (nrem_clean_combined_file, "rt") as edit_file2:
                edit_lines2 = edit_file2.readlines()
                counter3 = len(edit_lines2)
            qc_nrem_clean_combined_file = "QC_" + nrem_clean_combined_file
            i = 0 
            Ncount = 0 
            with open (qc_nrem_clean_combined_file, "wt") as outfile3:
                while i < counter3:
                    read_num = i + 1
                    qual_num = i + 3
                    if len(edit_lines2[read_num]) != len(edit_lines2[qual_num]):
                        ##print(edit_lines[i].find("N"))
                        i = i + 4
                        Ncount = Ncount + 1
                    else : 
                        line1 = edit_lines2[i]
                        i = i + 1
                        line2 = edit_lines2[i]
                        i = i + 1
                        line3 = edit_lines2[i]
                        i = i + 1
                        line4 = edit_lines2[i]
                        write_line2 = line1 + line2  + line3 + line4 
                        outfile3.write(write_line2)
                        i = i + 1
            print(str(Ncount) + " reads removed due to quality score artifacts")
            ### build quality score filtering routine here ### 
            ### the q and p params for fastx could be paramaterized here in a future version ###
            q30_file = "Q30_" + qc_nrem_clean_combined_file
            qual_filter_cmd = ["/usr/prog/ngdx/fastx/0.0.13/bin/fastq_quality_filter", "-q", "30", "-p" , "95" , "-i", qc_nrem_clean_combined_file , "-o" , q30_file , "-Q33"]
            subprocess.call(qual_filter_cmd)
            ### bowtie2 alignment calls ###
            sam_file_ref1 = "REF1_" + q30_file +".aln.sam"
            bowtie_ref1_call_cmd = ["bowtie2", "-x", ref1, "--trim3",R2_len , "--very-sensitive-local", "-U" , q30_file ,"-S" ,sam_file_ref1, "--no-hd", "--no-sq"]
            subprocess.call(bowtie_ref1_call_cmd)
            sam_file_ref2 = "REF2_" + q30_file + ".aln.sam"
            bowtie_ref2_call_cmd = ["bowtie2", "-x" ,ref2, "--trim3",R2_len , "--very-sensitive-local", "-U" , q30_file ,"-S" ,sam_file_ref2, "--no-hd", "--no-sq"]
            subprocess.call(bowtie_ref2_call_cmd)
            ### chopped out section that turned sam files into .tsv ###
            fq_tsv = "BC_" + q30_file + ".tsv" 
            with open(q30_file, "rt") as pasted_file:
                fq_lines = pasted_file.readlines()
                counter6 = len(fq_lines)
            i= 0
            ###os.system("chmod 777 *")
            with open (fq_tsv, "wt") as outfile6:
                head_string = "Read" +"\t"+ "BC"+ "\n"
                outfile6.write(head_string)
                while i < counter6 :
                    i = i + 1
                    fq_append_read = fq_lines[i].strip("\n")[-12:]
                    i = i + 2
                    fq_append_qscore = fq_lines[i].strip("\n")[-12:]
                    i = i + 1
                    write_line = fq_append_read + "\t" + fq_append_qscore + "\n"
                    outfile6.write(write_line)
            #### NEED A SECTION THAT READS IN TSV files / makes dataframes ###
            pathname1 = os.popen("pwd").read().strip('\n') + "/" + sam_file_ref1
            pathname2 = os.popen("pwd").read().strip('\n') + "/" + sam_file_ref2
            pathname3 = os.popen("pwd").read().strip('\n') + "/" + fq_tsv
            cols1 = max([each.count('\t') for each in os.popen("cat " +  sam_file_ref1).read().split('\n')])
            cols2 = max([each.count('\t') for each in os.popen("cat " +  sam_file_ref2).read().split('\n')])
            i = 0 
            name_arr1 = []
            while i < cols1 :
                name_arr1.append(i)
                i = i +1
            newframe1 = pandas.read_table(pathname1,sep='\t',header=None,names=name_arr1,index_col=False).fillna("*")
            ###############
            i = 0
            name_arr2 = []
            while i < cols2 :
                name_arr2.append(i)
                i = i + 1
            newframe2 = pandas.read_table(pathname2,sep='\t',header=None,names=name_arr2,index_col=False).fillna("*")
            newframe3 = pandas.read_table(pathname3 , sep="\t").fillna("*")
            ###############
            i = 11
            str_eval1 = ''
            while i < cols1 : 
                str_eval1 = str_eval1 + "newframe1[newframe1.columns[" + str(i) + "]]" + " + "
                i = i +1
            str_eval1 = str_eval1[:-3]
            str_eval1 = str_eval1.replace("+","+ ';' +")
            newframe1['flags'] = eval(str_eval1)
            newframe1 = newframe1.drop(columns= newframe1.columns[11:cols1])
            ####
            i = 11
            str_eval2 = ''
            while i < cols2 : 
                str_eval2 = str_eval2 + "newframe2[newframe2.columns[" + str(i) + "]]" + " + "
                i = i +1
            str_eval2 = str_eval2[:-3]
            str_eval2 = str_eval2.replace("+","+ ';' +")
            newframe2['flags'] = eval(str_eval2)
            newframe2 = newframe2.drop(columns= newframe2.columns[11:cols2])
            merged_results = pandas.concat([newframe1, newframe2, newframe3], axis=1, join_axes=[newframe1.index])
            merged_results.columns= ['QNAME', 'FLAG', 'RNAME', 'POS', 'MAPQ', 'CIGAR', 'RNEXT', 'PNEXT','TLEN', 'SEQ', 'QUAL', 'flags', 'QNAME.1', 'FLAG.1', 'RNAME.1', 'POS.1', 'MAPQ.1', 'CIGAR.1', 'RNEXT.1','PNEXT.1', 'TLEN.1', 'SEQ.1', 'QUAL.1', 'flags.1', 'Read', 'BC']
            merged_results = merged_results.fillna("*")
            table_out = merged_results.groupby(['SEQ','RNAME','RNAME.1','POS','POS.1','flags','flags.1','Read'])['SEQ'].count()
            out_table_name = "RESULTS_" + file + ".csv"
            table_out.to_csv(out_table_name, header=True)
            ### insert a subprocess command ###