#!/usr/bin/env python

### cmd line params: 
### 1. csv worklist with smfids 
### 2. reference .fa file string root (filename w/o .fa)
### 3. L coordinates (e.g. 700-900)
### 4. KI coordinates (e.g. 1000-1500)
### 5. R coordinates (e.g. 1700-1900)
### 6. outfile name (will be a summary .csv for all files in worklist) 
###
import csv 
import sys
import fileinput 
import re
import os
import subprocess
import collections
import numpy
import pandas
import xlsxwriter
import xlrd


worklistFile = sys.argv[1]
refFile = sys.argv[2]
Lcoord = sys.argv[3]
KIcoord = sys.argv[4]
Rcoord = sys.argv[5]
outFile = sys.argv[6]

with open(worklistFile, 'rt') as infile:
	reader = csv.DictReader(infile)
	for row in reader:
		name = row["Sample SMF-ID"]
		file_list = []
		file_list += [ each for each in os.listdir(".") if each.startswith(name) and each.find("_R1_") > 0 and each.endswith("sorted.bam") ]
		for file in file_list:
			print(file)
			print(name)
			left_cmd = "samtools depth -l 50 -q 20 -Q 30 -r " + refFile + ":" + Lcoord + " " + file + " > " + name + "_left.txt"
			os.system(left_cmd)
			KI_cmd = "samtools depth -l 50 -q 20 -Q 30 -r " + refFile + ":" + KIcoord + " " + file + " > " + name + "_KI.txt"
			os.system(KI_cmd)
			right_cmd = "samtools depth -l 50 -q 20 -Q 30 -r " + refFile + ":" + Rcoord + " " + file + " > " + name + "_right.txt"
			os.system(right_cmd)
			pathname = os.popen("pwd").read().strip("\n")
			pathname_left = pathname + "/" + name + "_left.txt"
			leftframe = pandas.read_table(pathname_left , header=None, names="NPC",  sep="\t")
			pathname_right = pathname + "/" + name + "_right.txt"
			rightframe = pandas.read_table(pathname_right , header=None, names="NPC",  sep="\t")
			pathname_KI = pathname + "/" + name + "_KI.txt"
			KIframe = pandas.read_table(pathname_KI , header=None, names="NPC",  sep="\t")
			if numpy.isnan(leftframe['C'].mean()):
				left_mean = 0
			else: left_mean = leftframe['C'].mean()
			if numpy.isnan(rightframe['C'].mean()):
				right_mean = 0
			else: right_mean = rightframe['C'].mean()
			if numpy.isnan(KIframe['C'].mean()):
				KI_mean = 0
			else: KI_mean = KIframe['C'].mean()
			print(left_mean)
			print(KI_mean)
			print(right_mean)
