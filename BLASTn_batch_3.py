#!/usr/bin/env python


### 1 : .csv source file to read 
### 2 : column header name with sequence query names
### 3 : column header name with sequences to query
### 4 : genome reference pointer (hg38, mm10)
### 5 : desired outfile name 

import csv
import sys
import fileinput 
import re
import os
import subprocess
import pandas

print("creating .fa file")



def csvTofasta( file, seqname, seq):
    filename = "multifasta.fa"
    with open(file, 'rt') as f:
        reader = csv.DictReader(f)
        ### make one multi-fasta instead ###
        fastalist = list()
        with open (filename,'wt') as ofile1:
            for row in reader:
                header = ">" + row[seqname]
                seq_row = row[seq] + "\n"
                text = [header , seq_row ]
                filename = row[seqname] +".fa"
                outstring = "\n".join(text)
                fastalist.append(outstring)
            for fasta in fastalist:
                    ofile1.write(fasta)
    return;

csvTofasta(sys.argv[1], sys.argv[2], sys.argv[3])

if sys.argv[4] == "hg38":
    genome = "/db/nibrgenome/NG00009.0/indexes/blast/hg38"
elif sys.argv[4] == "mm10": 
    genome = "/db/nibrgenome/NG00009.0/indexes/blast/mm10"
elif sys.argv[4] == "zf" :
    genome = "/home/clarksc3/zf/BLAST/Danio_rerio_ASI_v1.6.fasta"

subprocess.run(["blastn","-query","multifasta.fa","-db",genome, "-task", "blastn-short", "-outfmt","10 qseqid qseq stitle qlen qstart qend sstart send pident nident sstrand", "-out", "temp.txt", "-word_size", "17", "-dust", "no",  "-num_threads", "32", "-ungapped"], encoding = 'utf-8')
list_df = pandas.read_table("temp.txt",sep=",",header=None)
list_df.columns=["QueryName","QuerySeq","MatchID","QueryLen","QueryStart","QueryEnd","MatchStart","MatchEnd","PercentIdentity","NumIdent","Strand"]
### drop empty row that comes along with splitting on final newline character when list was built"
list_df = list_df[list_df["QueryName"] != ""]
#print(list_df)
outfile= sys.argv[5] + ".csv"
list_df.to_csv(outfile)





