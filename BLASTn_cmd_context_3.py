#!/usr/bin/env python


### 1 : .csv source file to read 
### 2 : column header name with sequence query names
### 3 : column header name with sequences to query
### 4 : genome reference pointer (hg38, mm10)
### 5 : desired outfile name 

import csv
import sys
import fileinput 
import re
import os
import subprocess

print("creating .fa files")

def csvTofasta( file, name, seq):
    file = sys.argv[1]
    with open(file, 'rt') as f:
     reader = csv.DictReader(f)
     readerIndex = sys.argv[2]
     readerSeq = sys.argv[3]
     for row in reader:
        header = ">" + row[readerIndex]
        seq = row[readerSeq] + "\n"
        text = [header , seq ]
        filename = row[readerIndex] +".fa"
        ofile1 = open(filename, "w+t")
        outstring = "\n".join(text)
        ofile1.write(outstring)
    return;

        

csvTofasta(sys.argv[1], sys.argv[2], sys.argv[3])

if sys.argv[4] == "hg38":
    genome = "/db/nibrgenome/NG00009.0/indexes/blast/hg38"
elif sys.argv[4] == "mm10": 
    genome = "/db/nibrgenome/NG00009.0/indexes/blast/mm10"
elif sys.argv[4] == "zf" :
    genome = "/home/clarksc3/zf/BLAST/Danio_rerio_ASI_v1.6.fasta"



### cmd line BLASTn subprocess routine

print("compiling initial BLASTn results table")


with open("temp.csv" , "at") as csvFile:
    writer = csv.writer(csvFile, delimiter = ',')
    writer.writerow(["QueryName","QuerySeq","MatchID","QueryLen","QueryStart","QueryEnd","MatchStart","MatchEnd","PercentIdentity","NumIdent","Strand"])
    list = []
    list += [ each for each in os.listdir(".") if each.endswith(".fa")]
    for fileFA in list: 
        string_new = subprocess.run(["blastn","-query",fileFA,"-db",genome, "-task", "blastn-short", "-outfmt","10 qseqid qseq stitle qlen qstart qend sstart send pident nident sstrand", "-word_size", "17", "-dust", "no",  "-num_alignments", "1", "-perc_identity", "100", "-max_hsps", "2", "-num_threads", "32", "-ungapped"], encoding = 'utf-8', stdout=subprocess.PIPE).stdout.strip('\n')
        csvText = string_new.split(",")
        writer.writerow(csvText)
        


outFile = sys.argv[5]


if sys.argv[4] == "hg38":
    genome = "/db/nibrgenome/NG00009.0/fasta/hg38.fa"
elif sys.argv[4] == "mm10": 
    genome = "/db/nibrgenome/NG00009.0/fasta/mm10.fa"
    
print ("outputting final query")

with open(outFile, 'at') as out:
    writer2 = csv.writer(out)
    writer2.writerow(["QueryName","QuerySeq","MatchID","QueryLen","QueryStart","QueryEnd","MatchStart","MatchEnd","PercentIdentity","NumIdent","Strand","Coord","PaddedSeq"])
    with open('temp.csv', 'rt') as infile:
         reader = csv.DictReader(infile)
         for row in reader:
            name = row["QueryName"]
            seq = row["QuerySeq"]
            chr = row["MatchID"]
            qLen = row["QueryLen"]
            qstart = row["QueryStart"]
            qend = row["QueryEnd"]
            start = row["MatchStart"]
            end = row["MatchEnd"]
            pident = row["PercentIdentity"]
            numI = row["NumIdent"]
            strand = row["Strand"]
            if strand == "plus":
                startN = int(start) - 3
                endN = int(end) + 3
                chr_coord = chr + ":" + str(startN) + "-" + str(endN) 
                # pysam keeps throwing inconsistent errors
                #coord = pysam.faidx(genome,chr_coord)[0].rstrip('\n').strip('>')
                #seqPad = pysam.faidx(genome,chr_coord)[1].rstrip('\n').strip('>')
                coord = subprocess.run(["samtools","faidx",genome,chr_coord],encoding = 'utf-8', stdout=subprocess.PIPE).stdout.split("\n")[0].strip(">")
                seqPad = subprocess.run(["samtools","faidx",genome,chr_coord],encoding = 'utf-8', stdout=subprocess.PIPE).stdout.split("\n")[1].strip(">")
                textJ = [ name, seq, chr, qLen, qstart, qend, start, end, pident, numI, strand, coord, seqPad]
                writer2.writerow(textJ)
            if strand == "minus":
                startN = int(start) + 3
                endN = int(end) -3
                chr_coordR = chr + ":" + str(endN) + "-" + str(startN)
                #pysam keeps throwing inconsistent errors
                #coord = pysam.faidx(genome,chr_coordR)[0].rstrip('\n').strip('>')
                #seqPad = pysam.faidx(genome,chr_coordR)[1].rstrip('\n').strip('>')
                coord = subprocess.run(["samtools","faidx",genome,chr_coordR],encoding = 'utf-8', stdout=subprocess.PIPE).stdout.split("\n")[0].strip(">")
                seqPad = subprocess.run(["samtools","faidx",genome,chr_coordR],encoding = 'utf-8', stdout=subprocess.PIPE).stdout.split("\n")[1].strip(">")
                textJ = [ name, seq, chr, qLen, qstart, qend, start, end, pident, numI, strand, coord, seqPad]
                writer2.writerow(textJ)

print ("done")

