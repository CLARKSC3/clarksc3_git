### this assumes Biopython has been imported ###
### from Bio.Seq import Seq ###
def rc(x):
     seq = Seq(x)
     return str(seq.reverse_complement())