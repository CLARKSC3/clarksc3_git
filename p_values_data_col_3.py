#!/usr/bin/env python

import pandas
from scipy import stats
import os
import csv
import sys

### feed the script the name of the source data file (e.g. QIAseqUltraplexRNA_2544_barcodes_Zscores.txt) ###
datafile = sys.argv[1]
### feed script the name of the control dataset here (e.g. DMSO_0.00) ###
Ref_set = sys.argv[2]
data_col = sys.argv[3] 
#### read in datafile to pandas dataframe ###
### this assumes tab delimited UTF-8 encoded text file ###
newdf = pandas.read_table(datafile,sep='\t')
#newdf['C_C_G'] = newdf['Compound_Concentration'] + newdf['gene']
### make list of all values; make unique with set; convert set back into indexable list ###
newlist = list(newdf['C_C_G'])
newset = set(newlist)
ulist = list(newset)
### Initialize empty dataframe to collect calculations ###
calcs = pandas.DataFrame()
### make list of all unique gene values ###
genelist = list(newdf['gene'])
geneset = set(genelist)
ugenelist = list(geneset)
### make Control values table ###
for gene in ugenelist:
    #print(gene)
    Ref_set_name = Ref_set + gene
    RefDataFrame = newdf[newdf['C_C_G'] == Ref_set_name]
    ### make unique list of C_C_G values matching gene ###
    C_C_G_list = [each for each in newdf['C_C_G'] if each.endswith(gene)]
    CCG_set = set(C_C_G_list)
    u_C_C_G_list = list(CCG_set)
    #print(len(calcs))
    ### loop through C_C_G_list and build p value matrix ###
    for CCG in u_C_C_G_list:
        #print(CCG)
        ### make dataframe slice of just matching CCG data ###
        CCG_df = newdf[newdf['C_C_G'] == CCG]
        calcs_var = stats.ttest_ind(CCG_df[data_col],RefDataFrame[data_col],equal_var = False)
        pval = calcs_var[1]
        #print(pval)
        new_item = {'C_C_G':CCG,'pval':pval}
        calcs = calcs.append(new_item,ignore_index=True)
calcs.to_csv("calcs.csv")

