#!/usr/bin/env python


### cmd line params: 
### 1. worklistfilename 
### 2. output filename  


import numpy
import pandas
import xlsxwriter
import xlrd
import os
import sys
import csv
import re


## get(base , base) in get avoids null data errors
## define reverse_complement
def rc(seq):
    complement = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A', 'a':'t', 'c':'g', 'g':'c', 't':'a'}
    reverse_complement = "".join(complement.get(base, base) for base in reversed(seq))
    return reverse_complement;
def alignment_exp(x,y):
    cmd_str = "/usr/prog/bioinf/emboss/current/bin/stretcher -asequence asis:" + x +" -bsequence asis:" + y +" -aformat3 fasta -stdout -auto"
    return_list = str(os.popen(cmd_str).read()).replace('\n','').replace('>asis','\n>asis\n').split('\n')
    ret_seq1 = return_list[2]
    ret_seq2 = return_list[4]
    ret_arr1 = list(ret_seq1)
    ret_arr2 = list(ret_seq2)
    ret_arr3 = []
    counter = len(ret_seq1)
    i = 0
    ### logic for insertions and SNV's...works for deletions too? ###
    while i < counter:
        if ret_arr1[i] == ret_arr2[i]:
            ret_arr3.append(ret_arr2[i])
        else:
            ret_arr3.append(ret_arr2[i].lower())
        i = i +1
        ret_seq2 = "".join(ret_arr3)
    return [ret_seq1,ret_seq2];

## variable to read worklistfilename ##
worklistfilename = sys.argv[1]

### create empty summary dataframe to be filled ###

summary_var = pandas.DataFrame(columns= ['FILE','COUNT'])

with open(worklistfilename, 'rt') as f:
    reader = csv.DictReader(f)
    for row in reader:
        key = row["FileListKey"]
        amplicon = row["Amplicon"]
        PAM = int(row["PAM"])
        window = int(row["Window"])
        l_bound = int(PAM - (window / 2))
        r_bound = int(PAM + (window / 2))
        l_bound_r = int(l_bound + 10)
        r_bound_l = int(r_bound - 10)
        l_region = amplicon[l_bound:l_bound_r]
        r_region = amplicon[r_bound_l:r_bound]
        print(l_bound)
        print(r_bound)
        ref_region = amplicon[l_bound:r_bound]
        grep_string = l_region + ".*" + r_region
        rc_l_region = rc(l_region)
        rc_r_region = rc(r_region)
        rc_grep_string = rc_r_region + ".*" + rc_l_region
        #print(key)
        List = []
        List += [each for each in os.listdir(".") if each.find(key) > -1 and each.find("_R1_") > 0 and each.endswith(".fastq.gz")]
        #print(List)
        for file in List:
            file_int = file[:file.find("_R1_")]
            file2 = file_int + "_R2_001.fastq.gz"
            print(file)
            print(file2)
            pathname = os.popen("pwd").read().strip('\n') + "/" + file
            #print(grep_string)
            grep_cmd = "zgrep -i -o " + grep_string + " " + file 
            greplist = str(os.popen(grep_cmd).read()).split('\n')
            rc_grep_cmd = "zgrep -i -o " + rc_grep_string + " " + file 
            rc_greplist = str(os.popen(rc_grep_cmd).read()).split('\n')
            #print(rc_greplist)
            grep_cmd_2 = "zgrep -i -o " + grep_string + " " + file2
            greplist2 = str(os.popen(grep_cmd_2).read()).split('\n')
            rc_grep_cmd_2 = "zgrep -i -o " + rc_grep_string + " " + file2
            rc_greplist_2 = str(os.popen(rc_grep_cmd_2).read()).split('\n')
            greplist.extend(greplist2)
            rc_greplist.extend(rc_greplist_2)
            greplist.extend(greplist2)
            greplist_3 = []
            for each in rc_greplist:
                greplist_3.append(rc(each))
            for each in rc_greplist_2:
                greplist_3.append(rc(each))
            greplist.extend(greplist_3)
            df = pandas.DataFrame(greplist)
            df.columns = ['SEQ']
            df = df[df.SEQ != ""]
            #print(df)
            newvar = pandas.DataFrame(df.groupby(['SEQ']).SEQ.count())
            newvar.columns= ['COUNT']
            newvar['REF'] = ref_region
            newvar['FILE'] = file
            newvar = newvar.reset_index()
            if newvar.empty:
                continue
            newvar['ALN'] = newvar.apply(lambda x:alignment_exp(x['REF'],x['SEQ'])[1], axis = 1)
            newvar['LEN'] = newvar.apply(lambda x:(len(x['SEQ']) - len(x['REF'])), axis = 1)
            newvar['smfid']= key
            outfilename = file + "_" + str(PAM)  + "_" + str(window) + ".grep.csv"
            newvar.to_csv(outfilename, header=True)
            summary_var = pandas.concat([summary_var,newvar],sort=False)
summary_var.index.name = 'INDEX' 
sumfilename = sys.argv[2] + ".csv"
summary_var.to_csv(sumfilename,header=True)



#### this could be improved by implementing pairwise alignment / appending alignment results 

### loop through dataframe and align each SEQ to REF ###

### different routines for same length A:B, Alen > Blen , Alen < Blen , ins/del/SNV etc.

#clarksc3::rogue$ /usr/prog/bioinf/emboss/current/bin/stretcher -asequence asis:CCCTGCATACTCCAGGCAGAACCAATCCTTTCCTGGTGCTTTGCCATTGTATTTGTGACTTGTTT -bsequence asis:CCCTGCATACTCCAGGCAGAACCTTCCCTGGTGCTTTGCCATTTGTGACTTGTTT -aformat3 fasta -stdout -auto
#>asis
#CCCTGCATACTCCAGGCAGAACCAATCCTTTCCTGGTGCTTTGCCATTGTATTTGTGACT
#TGTTT
#>asis
#CCCTGCATACTCCAGGCAGAACC-----TTCCCTGGTGCTTTGCCATT-----TGTGACT
#TGTTT
#clarksc3::rogue$

#>>> df['ALN'] = df.apply(lambda x:alignment_exp(x['REF'],x['ALT'])[1], axis = 1)
#>>> df
#        REF      ALT       ALN
#0  ATCGATCG  ATCGATG  ATCGAT-G


