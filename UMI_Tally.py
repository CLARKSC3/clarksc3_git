#!/usr/bin/env python


### cmd line params: 
### 1. samfile 
### 2. barcode fastq file name
### 3. collapsed reads file 
### 4. output filename  

import csv 
import sys
import fileinput 
import re
import os
import subprocess

samfile = sys.argv[1]
barcode_fastq = sys.argv[2]
mapped_collasped = sys.argv[3]
outfile = sys.argv[4]

##### list comp build example: 
>>> [(x, y) for x in [1,2,3] for y in [3,1,4] if x != y]
[(1, 3), (1, 4), (2, 3), (2, 1), (2, 4), (3, 1), (3, 4)]


###strip .fa names and build list of reads:
 
with open(mapped_collasped, "rw") as reads_list: 
	fa_lines = reads_list.readlines()
	All_reads = []
	for fa_line in fa_lines:
		if fa_line.nstartswith(">"): 
			All_reads += fa_line
		
		
### read by read		
for a_read in All_reads:
	header_list = []
	header_list = subprocess.check_output(["grep",a_read,samfile])
	read_barcode_list = []
		for header_line in header_list:
			tag = line[:re.search("	",line).start()]
			tag_str = str(tag)
			umi = subprocess.check_output(["grep","-A1",tag_str,barcode_fastq]).split("\n")[1]
			read_barcode = a_read + "_" + umi
			read_barcode_list += read_barcode
			