#!/usr/bin/env python


### 1 source .csv file to read smf-id tags from

### 2 genome reference (THIS SHOULD BE PARAMATERIZED by variable)

### 3 output filename 


### I should add positional paramters for genome and maybe source file header 

### this is limited to looking at filename tags at the BEGINNINGS of FILENAMES

import csv 
import sys
import fileinput 
import re
import os
import subprocess

### hard-coded for .gz zipped files 


### could not get subprocess to play nicely: 

#subprocess.check_output(["bwa","mem","-t","16",genome,file,file2,">",outfile])


#genome = "/home/clarksc3/test/XP01920.fa"
#genome = "/db/nibrgenome/NG00009.0/indexes/bwa/hg38"

genome = sys.argv[2]


with open(sys.argv[1], 'rt') as infile:
	reader = csv.DictReader(infile)
	for row in reader:
		name = row["Sample SMF-ID"]
		file_list = []
		file_list += [ each for each in os.listdir(".") if each.startswith(name) and each.find("_R1_") > 0 and each.endswith(".fastq.gz")]
		for file in file_list:
			file_int = file[:file.find("_R1_")]
			file2 = file_int + "_R2_001.fastq.gz"
			outfile = file + ".aln.sam"
			sam_cmd = "bwa mem -t 16 " + genome + " " + file + " " + file2 + " > " + outfile
			os.system(sam_cmd)
			bamfile = outfile + ".bam"
			bam_cmd = "samtools view -bt " + genome + " -o " + bamfile + " " + outfile
			os.system(bam_cmd)
			bamfile_sorted = outfile + ".sorted.bam"
			bam_sort = "samtools sort" + " -o "+ bamfile_sorted + " " + bamfile
			os.system(bam_sort)
			bai_file_dups = bamfile_sorted + ".bai"
			bam_bai_dups = "samtools index " + bamfile_sorted + " " + bai_file_dups
			os.system(bam_bai_dups)
			idx_file_dups = bamfile_sorted + ".idxstats"
			idx_stats_dups = "samtools idxstats " + bamfile_sorted + " " + bai_file_dups +  " > " + idx_file_dups
			os.system(idx_stats_dups)
			
			
list2 = []

with open(sys.argv[1], 'rt') as infile2:
	reader2 = csv.DictReader(infile2)
	for row2 in reader2:
		name2 = row2["Sample SMF-ID"]
		list2 += [ each for each in os.listdir(".") if each.startswith(name2) and each.find("_R1_") > 0 and each.endswith(".fastq.gz.aln.sam.sorted.bam.idxstats")]

outfileName = sys.argv[3]
outfileExt = ".csv"
textJ = [outfileName, outfileExt]
outfile = "".join(textJ)



for file in list2: 

	name = file[:re.search(".fastq.gz.aln.sam.sorted.bam.idxstats",file).start()]

	for line in fileinput.input(file, inplace=True): 
		text = [ name , line ]
		outstring = "	".join(text)
		sys.stdout.write(outstring.format(l=line))
with open(outfile, "wt") as sumFile: 
	writer = csv.writer(sumFile)
	writer.writerow(["FileName","chr","chrLen","MappedReads","UnmappedReads"])
	for file in list2:
		with open(file,"rt") as idx:
			rows = csv.reader(idx, delimiter ="\t")
			for row in rows:
				writer.writerow(row)
	
	
	







