#!/usr/bin/env python

### primer design script ###


### command line parameters
### 1. Input file (.csv) : passes query names and query genomic loci
### 2. desired amplicon size
### 3. genome (make friendly inputs like hg38 or mm10 etc available)

import csv 
import sys
import fileinput 
import re
import os
import subprocess

infile = sys.argv[1]
amp_size = int(sys.argv[2])



if sys.argv[3] == "hg38":
	genome = "/db/nibrgenome/NG00009.0/fasta/hg38.fa"
elif sys.argv[3] == "mm10": 
	genome = "/db/nibrgenome/NG00009.0/fasta/mm10.fa"
elif sys.argv[3] == "zf" : 
	genome = "home/clarksc3/zf/Danio_rerio_ASI_v1.6.fasta"



with open("primer3_input_file.txt", "wt") as prim_file:
	with open(infile, "rt") as worklist:
		reader = csv.DictReader(worklist)
		for row in reader:
			tag = row["Tag"]
			seq = row["Sequence"]
			coord = row["Coordinates"]
			### parse coord string into longer version ###
			coord_chr = coord[:re.search(":",coord).start()]
			coord_new_string = coord[:re.search("-",coord).start()]
			coord_left_old = int(coord_new_string[re.search(":",coord_new_string).start()+1:])
			coord_right_old = int(coord[re.search("-",coord).start()+1:])
			query_string_len = int(coord_right_old - coord_left_old)
			left_lim = int(round(((amp_size - query_string_len) / -2),0))
			right_lim = int(round(((amp_size - query_string_len)/ 2),0))
			coord_left = int(coord_new_string[re.search(":",coord_new_string).start()+1:]) + left_lim
			coord_right = int(coord[re.search("-",coord).start()+1:]) + right_lim
			chr_string = coord_chr.strip() + ":" + str(coord_left).strip() + "-" + str(coord_right).strip() 
			### expression to extract just the sequence from a samtools faidx call
			Sequence = "".join(subprocess.run(["samtools","faidx",genome,chr_string], encoding = 'utf-8', stdout=subprocess.PIPE).stdout.splitlines()[1:])
			print("".join(Sequence.replace("\n","")))
			SEQUENCE_ID = "SEQUENCE_ID=" + tag.strip() + "_" + seq.strip() + "_" + coord.strip()
			### start routine to make primer3 input file 
			prim_file.write(SEQUENCE_ID + "\n")
			SEQUENCE_TEMPLATE = "SEQUENCE_TEMPLATE=" + Sequence.strip()
			prim_file.write(SEQUENCE_TEMPLATE + "\n")
			prim_file.write("PRIMER_TASK=generic" + "\n" + "PRIMER_PICK_LEFT_PRIMER=1" + "\n" + "PRIMER_PICK_INTERNAL_OLIGO=0" + "\n" + "PRIMER_PICK_RIGHT_PRIMER=1" + "\n" + "PRIMER_OPT_SIZE=20" + "\n" + "PRIMER_MIN_SIZE=18" + "\n" + "PRIMER_MAX_SIZE=22" + "\n")
			lower = amp_size - 50 
			upper = amp_size + 50
			prim_file.write("PRIMER_PRODUCT_SIZE_RANGE=" + str(lower).strip() + "-" + str(upper).strip() + "\n")
			prim_file.write("PRIMER_EXPLAIN_FLAG=1" + "\n" + "PRIMER_THERMODYNAMIC_PARAMETERS_PATH=/home/clarksc3/primer3/src/primer3_config/" + "\n" + "=" + "\n")

primer3_cmd = "/home/clarksc3/primer3/src/primer3_core --output=output.txt primer3_input_file.txt"
os.system(primer3_cmd)

with open("output.txt", "rt") as count_file: 
	count_lines = count_file.readlines()
	counter1 = len(count_lines)
	matching = []
	matching = [s for s in count_lines if "SEQUENCE_ID" in s]
	counter2 = len(matching)
	
with open("summary.csv" , "wt") as sumFile:
	writer = csv.writer(sumFile)
	writer.writerow(["ID","SEQ","L_Primer","R_Primer","L_TM","R_TM","Amp_Size"])
	for matches in matching :
		i = 0
		try:
			while count_lines[i] != matches :
				i = i + 1
			ID = ""
			ID = count_lines[i].split("=")[1].strip()
			print(ID)
			while bool(re.search("SEQUENCE_TEMPLATE",count_lines[i])) != True :
				i = i + 1
			SEQ = ""
			SEQ = count_lines[i].split("=")[1].strip()
			print(SEQ)
			while bool(re.search("PRIMER_PAIR_NUM_RETURNED",count_lines[i])) != True :
				i = i + 1
			NUM_RET = ""
			NUM_RET = int(count_lines[i].split("=")[1].strip())
			if NUM_RET == 0 :
				continue
			else:
				print("writing results")
				while bool(re.search("=\n",count_lines[i])) != True :
					while bool(re.search("PRIMER_LEFT_.*_SEQUENCE=*",count_lines[i])) != True :
							i = i + 1
					PRIMER_LEFT_SEQUENCE = count_lines[i].split("=")[1].strip().upper()
					while bool(re.search("PRIMER_RIGHT_.*_SEQUENCE=*",count_lines[i])) != True :
						i = i + 1
					PRIMER_RIGHT_SEQUENCE = count_lines[i].split("=")[1].strip().upper()
					while bool(re.search("PRIMER_LEFT_.*_TM=*",count_lines[i])) != True :
						i = i +1
					PRIMER_LEFT_TM = count_lines[i].split("=")[1].strip()
					while bool(re.search("PRIMER_RIGHT_.*_TM=*",count_lines[i])) != True :
						i = i +1
					PRIMER_RIGHT_TM = count_lines[i].split("=")[1].strip()
					while bool(re.search("PRIMER_PAIR_.*_PRODUCT_SIZE=*",count_lines[i])) != True :
						i = i +1
					PRIMER_PAIR_PRODUCT_SIZE = count_lines[i].split("=")[1].strip()
					print("writing")
					writer.writerow([ID,SEQ,PRIMER_LEFT_SEQUENCE,PRIMER_RIGHT_SEQUENCE,PRIMER_LEFT_TM,PRIMER_RIGHT_TM,PRIMER_PAIR_PRODUCT_SIZE])
					i = i + 1
		except IndexError :
			error_string = matches + " returned no acceptable primers"
			print(error_string)
		continue

with open("summary.csv" , "rt") as sumFile2:
	reader2 = csv.DictReader(sumFile2)
	with open("ispcr_input.txt" , "wt") as ispcr_file:
		for row in reader2:
			ID_out = row["ID"]
			L_SEQ = row["L_Primer"]
			R_SEQ = row["R_Primer"]
			ispcr_file.write(ID_out + "	" + L_SEQ + "	" + R_SEQ + "\n")

ispcr_string = "/usr/prog/bioinf/UCSC/bin/isPcr " + genome + " ispcr_input.txt ispcr_outfile"
os.system(ispcr_string)

with open("ispcr_outfile" , "rt") as sumFile3:
	out_lines = sumFile3.readlines()
	header_lines = [each for each in out_lines if bool(re.search(">",each))]
	with open("summary.csv" , "rt") as sumFile4:
		reader3 = csv.DictReader(sumFile4)
		with open("ispcr_results.csv" , "wt") as sumFile5:
			writer2 = csv.writer(sumFile5)
			writer2.writerow(["ID","L_Primer","R_Primer","L_TM","R_TM","Amp_Size","Coordinates"])
			for row in reader3:
				matching_header = [z for z in header_lines if bool(re.search(row["ID"],z)) and bool(re.search(row["L_Primer"],z)) and bool(re.search(row["R_Primer"],z)) and not bool(re.search(">.*_.*_alt:.* ",z))]
				matching_header_num = len(matching_header)
				print(matching_header_num)
				print(matching_header)
				if matching_header_num == 1 : 
					writer2.writerow([row["ID"],row["L_Primer"],row["R_Primer"],row["L_TM"],row["R_TM"],row["Amp_Size"],matching_header[0].strip()])